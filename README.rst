==============================================
PAF Python GribApi library
==============================================



9.2. Indexation: how to define an index
----------------------------------------

* [Ejercicio 1](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/6563e91fe3e3e103dc14617b0f7d9e2dc9532775/indexation_exercise_1.ipynb)

* [Ejercicio 1 resuelto](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/6563e91fe3e3e103dc14617b0f7d9e2dc9532775/indexation_exercise_1_resuelto.ipynb)



9.3. Iterators by grib keys values
-------------------------------------

* [Ejercicio 1](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/6563e91fe3e3e103dc14617b0f7d9e2dc9532775/keys_iterator_exercise_1.ipynb)

* [Ejercicio 1 resuelto](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/6563e91fe3e3e103dc14617b0f7d9e2dc9532775/keys_iterator_exercise_1_resuelto.ipynb)



9.4. GeoIterator
-----------------

* [Ejercicio 1](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/6563e91fe3e3e103dc14617b0f7d9e2dc9532775/geoiterator_exercise_1.ipynb)

* [Ejercicio 1 resuelto](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/6563e91fe3e3e103dc14617b0f7d9e2dc9532775/geoiterator_exercise_1_solucion.ipynb) 

* [Ejercicio 2](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/6563e91fe3e3e103dc14617b0f7d9e2dc9532775/geoiterator_exercise_2.ipynb)

* [Ejercicio 2 resuelto](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/6563e91fe3e3e103dc14617b0f7d9e2dc9532775/geoiterator_exercise_2_solucion.ipynb)

* [Ejercicio 3](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/6563e91fe3e3e103dc14617b0f7d9e2dc9532775/geoiterator_exercise_3.ipynb)

* [Ejercicio 3 resuelto](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/6563e91fe3e3e103dc14617b0f7d9e2dc9532775/geoiterator_exercise_3_solucion.ipynb)


Others
-------

* [Short tutorial on Basemap](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/398e1b9052133743a9560eea61a55b997ad6853d/Basemap%20tutorial.ipynb)

* [Short tutorial on Basemap 2](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/0040fb1080656dd1a9a0e1abb1bb0aefccacf68f/Short%20tutorial%20on%20Basemap%202.ipynb)

* [NetCDF: Scientific.IO.NetCDF](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-gribapi/raw/66b4c9889f3686c3e1c77c9cf199836f34360e36/netcdf.ipynb)
